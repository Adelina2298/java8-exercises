package answers.defaultmethods;

public interface A {
    default String printHello() {
        return "Hello A";
    }
}
