package answers.defaultmethods;

public class ABBA implements A, B {
    @Override
    public String printHello() {
        return A.super.printHello();
    }
}
