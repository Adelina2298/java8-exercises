package answers.defaultmethods;

public interface B {
    default String printHello() {
        return "Hello B";
    }
}
