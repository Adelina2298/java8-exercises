package answers.defaultmethods;

public interface SimpleNumberOp {

    public int getValue();

    static int add(SimpleNumberOp lhs, SimpleNumberOp rhs) {
        return lhs.getValue() + rhs.getValue();
    }

    default double getValueAsDouble() {
        throw new UnsupportedOperationException();
    }
}
