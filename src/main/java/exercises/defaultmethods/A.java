package exercises.defaultmethods;

public interface A {
    default String printHello() {
        return "Hello A";
    }
}
