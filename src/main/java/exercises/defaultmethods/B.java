package exercises.defaultmethods;

public interface B {
    default String printHello() {
        return "Hello B";
    }
}
