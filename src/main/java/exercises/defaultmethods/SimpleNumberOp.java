package exercises.defaultmethods;

public interface SimpleNumberOp {

    public int getValue();

}
