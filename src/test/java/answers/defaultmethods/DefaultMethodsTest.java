package answers.defaultmethods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DefaultMethodsTest {

    @Test
    public void static_method_can_add_two_simple_number_classes_values_together() {
        SimpleNumberOp lhs = new SimpleNumberOpImpl(16);
        SimpleNumberOp rhs = new SimpleNumberOpImpl(10);
        assertEquals(26, SimpleNumberOp.add(lhs, rhs));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void add_default_method_to_return_value_as_double_but_throws_an_exception() {
        SimpleNumberOp lhs = new SimpleNumberOpImpl(15);
        lhs.getValueAsDouble();
    }

    @Test
    public void can_print_the_result_from_A() {
        ABBA abba = new ABBA();
        assertEquals("Hello A", abba.printHello());
    }


}
