package answers.streams;

import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

public class FlatMapExercise {

    @Test
    public void flatteningLists() {
        Stream<List<Integer>> input = Stream.of(asList(1, 2), asList(3, 4));

        List<Integer> together = input
                .flatMap(Collection::stream)
                .collect(toList());

        assertEquals(asList(1, 2, 3, 4), together);
    }

}
